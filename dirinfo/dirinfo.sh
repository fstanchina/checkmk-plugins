#!/bin/bash
# Abuse fileinfo to show the size and age of directories.
# See https://checkmk.com/cms_check_fileinfo.html
#
# The configuration file "dirinfo.cfg" must contain a list of directories,
# one per line. Lines starting with # are ignored.
if [ -e "$MK_CONFDIR/dirinfo.cfg" ]; then
  echo '<<<fileinfo:sep(124)>>>'
  date '+%s'
  echo '[[[header]]]'
  echo 'name|status|size|time'
  echo '[[[content]]]'
  grep -v '^#' "$MK_CONFDIR/dirinfo.cfg" | while read d; do
    if [ -d "$d" ]; then
      # size (in bytes)
      # time (Unix timestamp)
      du --bytes --summarize --time --time-style='+%s' "$d" | while read s t n; do
        echo "directory:$n|ok|$s|$t"
      done
    else
      echo "directory:$d|missing"
    fi
  done
fi
