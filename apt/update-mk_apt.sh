#!/bin/sh
set -e
PLUGIN='/usr/lib/check_mk_agent/plugins/90000/mk_apt'
CACHE='/var/lib/check_mk_agent/cache/plugins_90000\mk_apt.cache'
if [ -x "$PLUGIN" ] && [ -f "$CACHE" ]; then
  test -t 1 && echo -n 'Updating mk_apt cache... '
  "$PLUGIN" > "$CACHE.new" && mv -f "$CACHE.new" "$CACHE"
  test -t 1 && echo 'Done'
fi
