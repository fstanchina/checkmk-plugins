Various plugins for [Checkmk](https://checkmk.com/).

## apt
Standard APT plugin, modified to **not** run `apt-get update` itself.

The plugin relies on APT hooks to refresh the package status:
- a hook refreshes the status after packages are installed/removed;
- another hook refreshes the status immediately after `apt update`.

This will update the status in Checkmk very quickly (within a minute),
instead of taking a long time.

The plugin script is installed into `plugins/90000` (runs every 25 hours)
to ensure that the agent runs it at least once a day if nothing else does.

It's recommended to install `apt-config-auto-update` to enable automatic
daily updates of the package list.

The rationale is that running once a day is good enough for this plugin.

## dirinfo
Abuse fileinfo to show the size and age of directories.
See https://checkmk.com/cms_check_fileinfo.html

You can use this plugin when all you really need is to keep an eye on a
directory and you want to skip the tedious configuration of file groups,
and also to reduce traffic between the agent and the server.

The plugin will report the total size and the time of the last modification
of any file in the directory, as determined by `du --time`.

## lvm
Standard LVM plugin + fake thin pools as VGs.

## openvpn-client
Local plugin to check OpenVPN client connection status.

## os-labels
Generate labels with OS information.
See https://docs.checkmk.com/latest/en/labels.html

_EXPERIMENTAL!_
Labels are a new feature in Checkmk version 1.6.0.

## reboot-notifier
Check when a system reboot is required due to a kernel update.

Plugs into the `reboot-notifier` mechanism.

## smart
Standard S.M.A.R.T. plugin with minor changes.

## systemtime
HACK: system time like the Windows agent.
