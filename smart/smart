#!/bin/bash
# +------------------------------------------------------------------+
# |             ____ _               _        __  __ _  __           |
# |            / ___| |__   ___  ___| | __   |  \/  | |/ /           |
# |           | |   | '_ \ / _ \/ __| |/ /   | |\/| | ' /            |
# |           | |___| | | |  __/ (__|   <    | |  | | . \            |
# |            \____|_| |_|\___|\___|_|\_\___|_|  |_|_|\_\           |
# |                                                                  |
# | Copyright Mathias Kettner 2014             mk@mathias-kettner.de |
# +------------------------------------------------------------------+
#
# This file is part of Check_MK.
# The official homepage is at http://mathias-kettner.de/check_mk.
#
# check_mk is free software;  you can redistribute it and/or modify it
# under the  terms of the  GNU General Public License  as published by
# the Free Software Foundation in version 2.  check_mk is  distributed
# in the hope that it will be useful, but WITHOUT ANY WARRANTY;  with-
# out even the implied warranty of  MERCHANTABILITY  or  FITNESS FOR A
# PARTICULAR PURPOSE. See the  GNU General Public License for more de-
# tails. You should have  received  a copy of the  GNU  General Public
# License along with GNU Make; see the file  COPYING.  If  not,  write
# to the Free Software Foundation, Inc., 51 Franklin St,  Fifth Floor,
# Boston, MA 02110-1301 USA.

# Only handle always updated values, add device path and vendor/model
if which smartctl > /dev/null 2>&1 ; then
    echo '<<<smart>>>'
    SEEN=
    for D in /dev/disk/by-id/{scsi,ata}-*; do
        [ "$D" != "${D%scsi-\*}" ] && continue
        [ "$D" != "${D%ata-\*}" ] && continue
        [ "$D" != "${D%-part*}" ] && continue
        N=$(readlink $D)
        N=${N##*/}
        if [ -r /sys/block/$N/device/vendor ]; then
            VEND=$(tr -d ' ' < /sys/block/$N/device/vendor)
        else
            # 2012-01-25 Stefan Kaerst CDJ - in case $N does not exist
            VEND=ATA
        fi
        if [ -r /sys/block/$N/device/model ]; then
            MODEL=$(sed -e 's/ /_/g' -e 's/_*$//g' < /sys/block/$N/device/model)
        else
            MODEL=$(smartctl -a $D | grep -i "device model" | sed -e "s/.*:[ ]*//g" -e "s/\ /_/g")
        fi
        # Excluded disk models for SAN arrays or certain RAID luns that are also not usable..
        if [ "$MODEL" = "iSCSI_Disk" -o "$MODEL" = "LOGICAL_VOLUME" ]; then
            continue
        fi

        # Avoid duplicate entries for same device
        if [ "${SEEN//.$N./}" != "$SEEN" ] ; then
            continue
        fi
        SEEN="$SEEN.$N."

        # strip device name for final output
        DNAME=${D#/dev/disk/by-id/scsi-}
        DNAME=${DNAME#/dev/disk/by-id/ata-}
        # 2012-01-25 Stefan Kaerst CDJ - special option in case vendor is AMCC
        CMD=
        if [ "$VEND" == "AMCC" -a -n "$TWAC" ]; then
            DNAME=${DNAME#1}
            [ -z "${!DNAME}" ] && continue
            CMD="smartctl -d 3ware,${!DNAME} -v 9,raw48 -A /dev/twa0"
            # create nice device name including model
            MODEL=$(tw_cli /$TWAC/p${!DNAME} show model | head -n 1 | awk -F= '{ print $2 }')
            MODEL=${MODEL## }
            MODEL=${MODEL// /-}
            DNAME=${DNAME#AMCC_}
            DNAME="AMCC_${MODEL}_${DNAME%000000000000}"
        elif [ "$VEND" != "ATA" ] ; then
            TEMP=
            # create temperature output as expected by checks/smart
            # this is a hack, TODO: change checks/smart to support SCSI-disks
            eval `smartctl -d scsi -i -A $D | while read a b c d e ; do
                [ "$a" == Serial ] && echo SN=$c
                [ "$a" == Current -a "$b" == Drive  -a "$c" == Temperature: ] && echo TEMP=$d
            done`
            [ -n "$TEMP" ] && CMD="echo 194 Temperature_Celsius 0x0000 000 000 000 Old_age Always - $TEMP (0 0 0 0)"
            DNAME="${VEND}_${MODEL}_${SN}"
        else
            CMD="smartctl -d ata -v 9,raw48 -A $D"
        fi

        [ -n "$CMD" ] && $CMD | grep Always | egrep -v '^190(.*)Temperature(.*)' | sed "s|^|$DNAME $VEND $MODEL |"
    done 2>/dev/null
else
    echo "ERROR: smartctl not found" >&2
fi
